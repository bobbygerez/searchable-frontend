## Getting started in Searchable Frontend
Clone the repository
`git clone https://bobbygerez@bitbucket.org/bobbygerez/searchable-frontend.git`

- Navigate to the cloned folder `cd searchable-frontend`
- Install dependencies `npm install`
- Run the server `npm run serve`
- Check if server is running  ` http://localhost:8080/`
