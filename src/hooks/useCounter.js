import { ref } from 'vue'

export default function useCounter(intialCount = 0, stepSize = 1) {
    const count = ref(intialCount);


    function incrementCount() {
        count.value += stepSize
    }

    return {
        count,
        incrementCount
    }
}