import { createApp } from 'vue'
import App from './App.vue'
import axios from 'axios'
import './index.css'
import './assets/tailwind.css'
import router from './router'
import store from './store'

const app = createApp(App)
app.use(router)
app.use(store)

const axiosConfig = {
    baseURL: 'http://127.0.0.1:8000/api',
    timeout: 30000,
};

app.config.globalProperties.axios = axios.create(axiosConfig)
app.mount('#app')