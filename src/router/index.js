import { createWebHistory, createRouter } from "vue-router";
import Home from "./views/Listings.vue";
import About from "./views/Paginate.vue";
import Composition from "./views/Composition.vue";
import ToRef from "./views/ToRef.vue";
import ReplacingMethods from './views/ReplacingMethods.vue'
import ReplacingComputed from './views/ReplacingComputed.vue'
import ReplacingWatch from './views/ReplacingWatch.vue'
import ProvideInject from './views/provide-inject/v2.vue'
import UnMounted from './views/unmounted.vue'
import cProps from './views/props/c_props.vue'
import cEvents from './views/events/index.vue'
import reusability from './views/reusability/index.vue'
const routes = [{
        path: "/",
        name: "Home",
        component: Home,
    },
    {
        path: "/about",
        name: "About",
        component: About,
    },
    {
        path: "/composition",
        component: Composition
    },
    {
        path: "/toref",
        component: ToRef
    },
    {
        path: '/replacingmethods',
        component: ReplacingMethods
    },
    {
        path: '/replacingcomputed',
        component: ReplacingComputed
    },
    {
        path: '/replacingwatch',
        component: ReplacingWatch
    },
    {
        path: '/provide-inject',
        component: ProvideInject
    },
    {
        path: '/unmounted',
        component: UnMounted
    },
    {
        path: '/props',
        component: cProps
    },
    {
        path: '/events',
        component: cEvents
    },
    {
        path: '/reusability',
        component: reusability
    }
];

const router = createRouter({
    history: createWebHistory(),
    routes,
});

export default router;