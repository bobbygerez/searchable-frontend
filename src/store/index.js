import { createStore } from "vuex"

const store = createStore({
    state: {
        categories: [],
        selectedCategory: '',
        min: 0,
        max: 50,
    },
    getters: {
        selectedCategory(state) {
            return state.selectedCategory
        },
        categories(state) {
            return state.categories
        },
        min(state) {
            return state.min
        },
        max(state) {
            return state.max
        }
    },
    mutations: {
        setSelectedCategory(state, payload) {
            state.selectedCategory = payload
        },
        setCategories(state, payload) {
            state.categories = payload
        },
        setMin(state, payload) {
            state.min = payload
        },
        setMax(state, payload) {
            state.max = payload
        }
    }
})

export default store